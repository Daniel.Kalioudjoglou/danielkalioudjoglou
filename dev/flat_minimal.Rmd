---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(readxl)

 library(openxlsx)
  library(purrr)

 sheet_names <- excel_sheets("data-raw/datasets.xlsx")
 nb_sheet <- as.numeric(length(sheet_names))
 map_lst(nb_sheet,function(.x) {read_xlsx("data-raw/datasets.xlsx",sheet= x)})
 datasets1 <- read_xlsx("data-raw/datasets.xlsx",sheet="voiture")


```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# imc
    
```{r function-imc}
#' imc
#' 
#' calcule l'indice de masse corporelle
#' 
#' @param masse poids de la personne
#' @param taille taille de la personne
#'
#' @return une valeur
#' 
#' @export
#' 
#' @exemple imc(masse = 80, taille = 1.80)
#' 
imc <- function(masse, taille){
  masse/(taille * taille)
  
}
```
  
```{r example-imc}
imc(masse = 80, taille = 1.80)

```
  
```{r tests-imc}
test_that("imc works", {
  expect_true(inherits(imc, "function")) 
})
```
  
# multi_import_excel
    
```{r function-multi_import_excel}
#' multi_imlport_excel
#' 
#' affiche une liste
#' 
#' @param path 
#'
#'@importFrom readxl read_xlsx
#'
#' @return une liste
#' 
#' @export
multi_import_excel <- function(path){
 



 datasets1 <- read_xlsx(path,sheet=1) 
 datasets2 <- read_xlsx(path,sheet=2) 
 datasets3 <- read_xlsx(path,sheet=3) 
 dataset <-  list (datasets1,datasets2,datasets3)
   
 
  
  
    
}
```
  
```{r example-multi_import_excel}
# multi_import_excel("data-raw/datasets.xlsx")
```
  
```{r tests-multi_import_excel}
test_that("multi_import_excel works", {
  expect_true(inherits(multi_import_excel, "function")) 
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_minimal.Rmd", vignette_name = "Minimal")
```
